﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEditor.Presets;
using UnityEngine.Experimental.PlayerLoop;
using Debug = UnityEngine.Debug;
using Object = System.Object;

//[AddComponentMenu("Lvl Design/Bob Factory")]

[ExecuteInEditMode]
[CustomEditor(typeof(LvlBuildScript))]
public class LvlBuildEditor : Editor
{
    static string nameStartButt = "Activate";

    private static bool isEnabled;

    static Transform  rootObj;

    public static Dictionary<string, ObjPreset> presets = new Dictionary<string, ObjPreset>();
    private static ObjPreset currentObjPreset;
    private static Material castMaterial;

    private const string areaLayerMask = "lvlBuildArea";
    private static int areaLayerMaskInt;
    private static Transform castTransform;

    public static float distanceCast = 10000;

    public static void Init(ObjPreset objPreset)
    {
        //currentObjPreset = new ObjPreset();
        areaLayerMaskInt = LayerMask.GetMask(areaLayerMask);

        if (castMaterial == null)
        {
            castMaterial = AssetDatabase.LoadAssetAtPath("Assets/LvlBuilder/Resources/LvlBuild/Material/spawn.mat", typeof(Material)) as Material;
        }

        if (presets.ContainsKey(objPreset.prefab.name))
        {
            currentObjPreset = presets[objPreset.prefab.name];
            currentObjPreset.SetUpPreset(objPreset);
            currentObjPreset.sectorContainer = GetSector(currentObjPreset);

            presets[objPreset.prefab.name] = currentObjPreset;
            Debug.Log("ContainsKey");
        }
        else
        {
            //add new obj
            objPreset.Output();
            currentObjPreset.SetUpPreset(objPreset); 

            presets.Add(currentObjPreset.prefab.name, currentObjPreset);
            CreateSector(currentObjPreset);
        }
       
    }

    static void CheckRoot()
    {
        if (rootObj != null)
        {
            return;
        }
        else
        {
            GameObject obj = GameObject.Find("TreeSystem");
            if (obj != null)
            {
                rootObj = obj.transform;
            }
            else
            {
                rootObj = SpawObj("TreeSystem");
            }
        }
    }
    static void CreateSector(ObjPreset objPreset)
    {
        CheckRoot();
        //check if exist
        GameObject sectorGameObject = GameObject.Find("Sector " + objPreset.prefab.GetInstanceID());
        if (sectorGameObject != null)
        {
            currentObjPreset.sectorContainer = sectorGameObject.transform;
        }
        else
        {
            currentObjPreset.sectorContainer = SpawObj("Sector " + objPreset.prefab.GetInstanceID());
            currentObjPreset.sectorContainer.SetParent(rootObj);
        }
        
    }

    static Transform GetSector(ObjPreset objPreset)
    {
        CheckRoot();
        if (currentObjPreset.sectorContainer == null)
        {
            Debug.Log("null sectorContainer");
            GameObject sectorGameObject = GameObject.Find("Sector " + objPreset.prefab.GetInstanceID().ToString());

            if (sectorGameObject == null)
            {
                Debug.Log("no sectorGameObject on scene");
                CreateSector(objPreset);
                return objPreset.sectorContainer;
            }
            else
            {
                return sectorGameObject.transform;
            }
        }

        else
        {
            return currentObjPreset.sectorContainer;
        }
    }
    static Transform SpawObj(string nameObj)
    {
        GameObject buffObj = new GameObject(nameObj);
        Undo.RegisterCreatedObjectUndo(buffObj, "Create " + nameObj);
        return buffObj.transform;
    }
    #region funcDel

    static void Activate()
    {
        nameStartButt = "Deactivate";

        isEnabled = true;

        currentObjPreset.Activate();
        SceneView.onSceneGUIDelegate += SpawnUpdate;

    }
    static void Deactivate()
    {
        nameStartButt = "Activate";
        SceneView.onSceneGUIDelegate -= SpawnUpdate;

        isEnabled = false;
        currentObjPreset.Deactivate();

    }

    #endregion funcDel
    static void SpawnUpdate(SceneView sceneview)
    {
        Event e = Event.current;

        RaycastHit hit;
        Ray pointToRay = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x, -e.mousePosition.y + Camera.current.pixelHeight));
        if (Physics.Raycast(pointToRay, out hit, distanceCast, areaLayerMaskInt, QueryTriggerInteraction.UseGlobal))
        {
            currentObjPreset.SetUpCastObj(hit.point);
        }

        if (Event.current.type == EventType.MouseDown && Event.current.button == 0) // create obj
        {
            Event.current.Use();
            if (Physics.Raycast(pointToRay, out hit, distanceCast, areaLayerMaskInt))
            {
                currentObjPreset.SpawnObj(hit.point);             
            }
        }
    }
    static void Visualize(SceneView sceneview)
    {
        Event e = Event.current;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LvlBuildScript myScript = (LvlBuildScript)target;

        if (GUILayout.Button("Open Obj Editor"))
        {
            LvlBuildWindow window = (LvlBuildWindow)EditorWindow.GetWindow(typeof(LvlBuildWindow));
        }

        if (GUILayout.Button(nameStartButt))
        {
            if(isEnabled)
            {
                Deactivate();
            }
            else
            {
                Activate();
            }
        }

        if (presets.Count > 0)
        {
            
            GUILayout.Label("Presets:");

            for (int i = 0; i < presets.Count; i++)
            {
                string nameKey = presets.ElementAt(i).Key;
                if (GUILayout.Button("Switch to " + nameKey))
                {
                    currentObjPreset = presets.ElementAt(i).Value;
                    //Activate();
                }

                if (GUILayout.Button("Edit "+ nameKey))
                {
                    LvlBuildWindow window = (LvlBuildWindow)EditorWindow.GetWindow(typeof(LvlBuildWindow));
                    window.SetUpElements(presets.ElementAt(i).Value);
                    window.Show();
                }
            }


            return;
            Debug.Log(presets.Count);

            float offset = 0;
            string key = "";
            for (int i = 0; i < presets.Count; i++)
            {
                if (GUILayout.Button(presets.ElementAt(i).Key))
                {
                    currentObjPreset = presets.ElementAt(i).Value;
                }

                ObjPreset objPreset = presets.ElementAt(i).Value;
                offset = objPreset.offset;
                EditorGUI.BeginChangeCheck();
                key = presets.ElementAt(i).Key; 

                offset = EditorGUILayout.FloatField("Offset:", offset);

                if (EditorGUI.EndChangeCheck())
                {
                    // Do something when the property changes 
                    if (presets.ElementAt(i).Value.offset != offset)
                    {
                        //Debug.Log("Offset !");
                        //presets[objPreset.prefab.name].SetOffset(offset);
                        ////presets.ElementAt(i).Value.SetOffset(offset);
                        //Repaint();
                        key = presets.ElementAt(i).Key;
                        break;

                    }
                }


            }

            presets[key].SetOffset(offset);
            Repaint();


        }
    }

    public static void  ChangeOffset(ObjPreset objPreset, float oldValue)
    {
        Transform[] presetsTransform = GetSector(objPreset).GetComponentsInChildren<Transform>();

        for (int i = 0; i < presetsTransform.Length; i++)
        {
            Vector3 currentPosition = presetsTransform[i].position;
            currentPosition -= Vector3.up * oldValue;
            currentPosition += Vector3.up * objPreset.offset;

            //currentPosition.y -= oldValue;// reset to origin state;
            //currentPosition.y += objPreset.offset; // set new offset
            presetsTransform[i].position = currentPosition;
        }
    }
    void AddTag()
    {

        // Open tag manager
        SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
        SerializedProperty tagsProp = tagManager.FindProperty("tags");

        // For Unity 5 we need this too
        SerializedProperty layersProp = tagManager.FindProperty("layers");

        // Adding a Tag
        string s = "the_tag_i_want_to_add";

        // First check if it is not already present
        bool found = false;
        for (int i = 0; i < tagsProp.arraySize; i++)
        {
            SerializedProperty t = tagsProp.GetArrayElementAtIndex(i);
            if (t.stringValue.Equals(s)) { found = true; break; }
        }

        // if not found, add it
        if (!found)
        {
            tagsProp.InsertArrayElementAtIndex(0);
            SerializedProperty n = tagsProp.GetArrayElementAtIndex(0);
            n.stringValue = s;
        }

        // Setting a Layer (Let's set Layer 10)
        string layerName = "the_name_want_to_give_it";

        // --- Unity 5 ---
        SerializedProperty sp = layersProp.GetArrayElementAtIndex(10);
        if (sp != null) sp.stringValue = layerName;
        // and to save the changes
        tagManager.ApplyModifiedProperties();
    }

    [Serializable]
    public struct ObjPreset
    {
        public GameObject prefab;
        public float offset;
        public Transform area;
        public Transform sectorContainer;
        private int originLayer;
        public GameObject RayCastSpawnObj;

        public void SetUpPreset(GameObject prefab, float offset, Transform area)
        {
            this.prefab = prefab;
            this.offset = offset;
            this.area = area;
            this.originLayer = prefab.layer;
        }

        public void SetOffset(float offset)
        {
            Debug.Log(this.offset + " <--> " + offset);
            this.offset = offset;
        }

        public void SetUpPreset(ObjPreset objPreset)
        {
            this.prefab = objPreset.prefab;
            this.offset = objPreset.offset;
            this.area = objPreset.area;
            this.originLayer = prefab.layer;
        }

        public void SetUpPreset(Object prefab, float offset, Object area)
        {
            this.prefab = (GameObject)prefab;
            this.offset = offset;
            this.area = area as Transform;
            this.originLayer = this.prefab.layer;
        }

        public void Output()
        {
            Debug.Log(prefab.name + " " + offset + " " + area.name);
        }

        public GameObject SpawnObj(Vector3 pointSpawn)
        {
            pointSpawn.y += offset;
            GameObject obj = Instantiate(prefab, sectorContainer);
            obj.transform.position = pointSpawn;
            obj.name = "Test[ " + pointSpawn.ToString() + " ]";
            Undo.RegisterCreatedObjectUndo(obj, "Create " + obj.name);
            return obj;
        }

        public void Deactivate()
        {
            area.gameObject.layer = originLayer;
            if (RayCastSpawnObj != null)
            {
                RayCastSpawnObj.SetActive(false);
            }
        }

        public void SetUpCastObj(Vector3 position)
        {
            RayCastSpawnObj.name = position.ToString();
            RayCastSpawnObj.transform.position = position;
            RayCastSpawnObj.transform.position += Vector3.up * offset;
        }

        public void Activate()
        {

            if (RayCastSpawnObj == null)
            {
                RayCastSpawnObj = Instantiate(prefab, sectorContainer);
                RayCastSpawnObj.GetComponent<Renderer>().material = castMaterial;
                Collider collider = RayCastSpawnObj.GetComponent<Collider>();

                if (collider != null)
                {
                    collider.enabled = false;
                }
            }
            else
            {
                RayCastSpawnObj.SetActive(true);
            }

            area.gameObject.layer = LayerMask.NameToLayer(areaLayerMask);
        }
    }
}

