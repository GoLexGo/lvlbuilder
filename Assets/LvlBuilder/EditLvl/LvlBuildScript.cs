﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class LvlBuildScript : MonoBehaviour
{
    static Object[] spawnObj = new Object[4];
#if UNITY_EDITOR
    public static bool Active;
    private void OnEnable()
    {
        Active = true;
    }
    public static Object GetObjById(int id)
    {
        return spawnObj[id];
    }

    public void Init()
    {
        spawnObj = AssetDatabase.LoadAllAssetsAtPath("AssetDatabase/Resources/LvlBuild");
    }

    private void OnDestroy()
    {
        Active = false;
    }
#endif

}
