﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LvlBuildWindow : EditorWindow
{//LvlBuildWindow
    float OffsetValue =0.0f;

    string status = "Insert obj";
    string Button = "Start";
    bool recording = false;
    float lastFrameTime = 0.0f;
    int capturedFrame = 0;
    private GameObject obj;
    Transform area;
    int selectedDropDown = 0;

    private float oldOffset;
    private bool changeMode;

    void OnGUI()
    {
        EditorGUILayout.Separator();
        
        OffsetValue = EditorGUILayout.FloatField("Offset:", OffsetValue);
        EditorGUILayout.LabelField("Object to Instantiate");
        obj =  (GameObject)EditorGUILayout.ObjectField(obj, typeof(GameObject), false);
        EditorGUILayout.LabelField("Area where u want to Instantiate obj");

        area =(Transform) EditorGUILayout.ObjectField(area, typeof(Transform), true);
        if (GUILayout.Button(Button))
        {
            if (obj != null && area != null)
            { 
                status = "Ready to start ";
                LvlBuildEditor.ObjPreset preset = new LvlBuildEditor.ObjPreset();
                preset.SetUpPreset(obj , OffsetValue, area);
                Debug.Log(preset.offset);
                LvlBuildEditor.Init(preset);
                if (changeMode)
                {
                    LvlBuildEditor.ChangeOffset(preset, oldOffset);
                    changeMode = false;
                }
                this.Close();
            }
          
        }
        EditorGUILayout.LabelField("Status: " , status);
    }

    public void SetUpElements(LvlBuildEditor.ObjPreset objPreset)
    {
        changeMode = true;
        oldOffset = objPreset.offset;
        obj = objPreset.prefab;
        OffsetValue = objPreset.offset;
        area = objPreset.area;
    }

    private void OnSelectionChange()
    {
        Debug.Log("change");
    }

    }
    


