﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LvlBuildInit :Editor
{
    [MenuItem("Lvl Builder/Initial Plugin")]
    public static void InitPlug()
    {
        if (!LvlBuildScript.Active)
            Instantiate(AssetDatabase.LoadAssetAtPath("Assets/LvlBuilder/Resources/LvlBuild/Prefab/LvlBuilder.prefab", typeof(GameObject)));
        else
            Debug.Log("It's already created");
        
    }
}
